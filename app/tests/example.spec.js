const { test, expect } = require("@playwright/test");

const playwright = require("playwright");

const chromium = require("playwright").chromium;
const webkit = require("playwright").webkit;
const devices = require("playwright").devices;
/*
test("basic test", async ({ page }) => {
  await page.goto("https://playwright.dev/");
  const title = page.locator(".navbar__inner .navbar__title");
  await expect(title).toHaveText("Playwright");
});
*/

//const chromium = require("playwright").chromium;
/*
test("basic test2", async ({ page }) => {
  const browser = await webkit.launch(); // or 'firefox','chromium'
  const iPhone11 = devices["iPhone 11"];
  const url = "https://www.google.com";
  //const url = 'https://www.taiyo-seimei.co.jp/net_lineup/'
  const context = await browser.newContext({
    ...iPhone11,
    locale: "ja",
  });
  //const page = await context.newPage();
  page = await context.newPage();
  await page.goto(url);
  await page.screenshot({ path: "data/example.png", fullPage: true });

  //const title = page.locator(".navbar__inner .navbar__title");
  //await expect(title).toHaveText("Google");

  await browser.close();
});
*/
/*
test("Test name", async ({ page }) => {
  // ここにテストコードを記述
});
*/
let mapX_start = 35.86575;// + 0.4;

test("basic test samson", async ({ page }) => {

    let mapX = mapX_start;

    for (const deviceName of ["iPad Pro"]) {
      const deviceType = devices[deviceName];
      const browserType = "chromium";

      for (let i = 0; i < 2; i++) {
        //    for (const browserType of ["chromium", "firefox", "webkit"]) {
        const browser = await playwright[browserType].launch();
        //const context = await browser.newContext()
        //const iPhone11 = devices["iPhone 11"];
        const context = await browser.newContext({
          ...deviceType,
          locale: "ja",
        });
        const page = await context.newPage();
        //#info_modal > div.iziModal-header.iziModal-noSubtitle > div
        await page.goto("https://9db.jp/dqwalk/map#"+ (mapX + 0.01*i) + ",139.70306,17");
        await page.click(".iziModal-header-buttons");
        await page.screenshot({ path: `data/DQW_A_${(mapX + 0.01*i) }.png` });
        await browser.close();
      }
    }
  });

