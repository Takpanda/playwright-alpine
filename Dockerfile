FROM mcr.microsoft.com/playwright

COPY ./app/ /usr/src/app/

WORKDIR /usr/src/app

# 日本語対応
RUN apt-get update && \
    apt-get -y install locales fonts-ipafont fonts-ipaexfont && \
    echo "ja_JP UTF-8" > /etc/locale.gen && locale-gen
    
# playwrightインストール
RUN npm i -D playwright
RUN npm i -D @playwright/test

#https://elaichenkov.medium.com/allure-report-integration-with-playwright-8c1570c67dda
#Java
#apt-get install default-jdk


# script実行
#CMD bash -c "node script.js"
CMD npx playwright test

#npx allure generate ./allure-results --clean